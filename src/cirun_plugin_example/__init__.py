from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__all__ = [
    'start_job',
    'monitor_job',
    'kill_job',
]

from datetime import datetime, timedelta
import random

from gitlab_runner_api import failure_reasons as gitlab_failure_reasons
import dateutil.parser

GET_MULTIPLE_JOBS_PER_CYCLE = True
JOB_REQUEST_WAIT = 15
JOB_MONITOR_WAIT = 10


def start_job(job):
    """Start a job which will run for 60 seconds and fail 50% of the time

    Parameters
    ----------
    job: gitlab_runner_api.Job

    Returns
    -------
    still_running: bool
        True if the job is still running
    state: JSON serialisable object
        Will be passed to future calls to `monitor_job` and `kill_job`
    """
    state = {
        'start_time': datetime.now().isoformat()
    }
    job.log += 'Starting example job which will last 60 seconds and has a 20% failure rate\n'
    still_running = True
    return still_running, state


def monitor_job(job, state):
    """Update the status of an already started GitLab CI job

    Parameters
    ----------
    job: gitlab_runner_api.Job
    state: object
        Object returned by the previous call to `start_job` or `monitor_job`

    Returns
    -------
    still_running: bool
        True if the job is still running
    state: JSON serialisable object
        Will be passed to future calls to `monitor_job` and `kill_job`
    """
    start_time = dateutil.parser.parse(state['start_time'])
    end_time = start_time + timedelta(seconds=60)
    if datetime.now() > end_time:
        still_running = False
        job.log += 'Job finished, randomly picking status\n'
        if random.random() > 0.2:
            job.set_success()
        else:
            job.set_failed(gitlab_failure_reasons.ScriptFailure())
    else:
        still_running = True
        job.log += 'Job is still running, will end at ' + end_time.isoformat() + '\n'
    return still_running, state


def kill_job(state):
    """Clean external state if the job is killed

    Parameters
    ----------
    state: object
        Object returned by the previous call to `start_job` or `monitor_job`
    """
    pass
