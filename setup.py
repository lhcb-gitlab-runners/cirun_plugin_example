from setuptools import setup, find_packages
from os.path import abspath, dirname, join
from io import open

here = abspath(dirname(__file__))

# Get the long description from the README file
with open(join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='cirun_plugin_example',
    use_scm_version=True,
    description='Example plugin for cirun',
    long_description=long_description,
    long_description_content_type='text/markdown',

    url='https://gitlab.cern.ch/lhcb-gitlab-runners/cirun_plugin_example',

    author='LHCb',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    keywords='cirun',

    packages=find_packages('src'),
    package_dir={'': 'src'},
    python_requires='>=2.7, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, !=3.4.*, !=3.5.*',
    setup_requires=['setuptools_scm'],
    install_requires=[
        'cirun',
        'gitlab_runner_api',
        'python-dateutil',
    ],
    zip_safe=True,

    entry_points={
        'cirun_plugins': {
            'example = cirun_plugin_example',
        }
    },

    project_urls={
        'Bug Reports': 'https://gitlab.cern.ch/lhcb-gitlab-runners/cirun_plugin_example/issues',
        'Source': 'https://gitlab.cern.ch/cburr/cirun_plugin_example',
    },
)
